.. _installation:

Installation
************

.. image:: images/demo.gif
   :width: 800

This document assumes the developer has a basic understanding of python
packaging, and how to install and manage python on the system executing
Molecule.

.. raw:: html

    <embed>
        <script id="asciicast-532969" src="https://asciinema.org/a/532969.js?t=10" async></script>
    </embed>

Requirements
============

URL
---

https://www.gitlab.com

Annotations
-----------

.. warning::
    Pensez à activer le **MFA** sur votre compte AWS, même sur un environnement de type *sandbox*.

.. danger::
   Beware killer rabbits!

.. tip::
   Specific admontions are rendered with a title matching the admonition type.

.. note::

    It can be particularly useful to pass the ``--destroy=never`` flag when
    invoking ``molecule test`` so that you can tell Molecule to run the full
    sequence but not destroy the instance if one step fails.

    If the ``--platform-name=[PLATFORM_NAME]`` flag is passed or the
    environment variable ``MOLECULE_PLATFORM_NAME`` is exposed when invoking
    ``molecule test``, it can tell Molecule to run the test in one platform
    only. It is useful if you want to test one platform docker image.

Tableau
-------

+------------+------------+-----------+ 
| Header 1   | Header 2   | Header 3  | 
+============+============+===========+ 
| body row 1 | column 2   | column 3  | 
+------------+------------+-----------+ 
| body row 2 | Cells may span columns.| 
+------------+------------+-----------+ 
| body row 3 | Cells may  | - Cells   | 
+------------+ span rows. | - contain | 
| body row 4 |            | - blocks. | 
+------------+------------+-----------+

Bullet lists
------------

- This is item 1
- This is item 2
- Bullets are "-", "*" or "+". Continuing text must be aligned after the bullet and whitespace.

Enumerated lists
----------------

1. This is the first item 
#. This is the second item 
#. Enumerators are arabic numbers, 
   single letters, or roman numerals 
#. List items should be sequentially 
   numbered, but need not start at 1 
   (although not all formatters will 
   honour the first index). 
#. This item is auto-enumerated

Footnotes
---------

They may be assigned 'autonumber 
labels' - for instance, 
[#first]_ and [#second]_.

Field lists
-----------

:Authors: 
    Tony J. (Tibs) Ibbs, 
    David Goodger
    (and sundry other good-natured folks)

:Version: 1.0 of 2001/08/08 
:Dedication: To my father.

Images
------

.. image:: images/6M6A2045.jpg
   :width: 600

Syntaxe
========

Block quotes
------------

Block quotes are just:
    Indented paragraphs,

        and they may nest.


Transitions
-----------

A transition marker is a horizontal line 
of 4 or more repeated punctuation 
characters.

------------

A transition should not begin or end a 
section or document, nor should two 
transitions be immediately adjacent.


Depending on the driver chosen, you may need to install additional OS packages.
See ``INSTALL.rst``, which is created when initializing a new scenario.

* Python >= 3.8 with Ansible >= 2.8

CentOS 8
--------

.. code-block:: bash

    $ sudo dnf install -y gcc python3-pip python3-devel openssl-devel python3-libselinux

Ubuntu 16.x
-----------

.. code-block:: bash

    $ sudo apt update
    $ sudo apt install -y python3-pip libssl-dev


Code
----

pip is the only supported installation method.

.. warning::

  Ansible is not listed as a direct dependency of molecule package because
  we only call it as a command line tool. You may want to install it
  using your distribution package installer.

  .. code-block:: bash

      $ python3 -m pip install molecule ansible-core




.. [#first] a.k.a. first_

.. [#second] a.k.a. second_

:Authors: 
    Tony J. (Tibs) Ibbs, 
    David Goodger
    (and sundry other good-natured folks)
